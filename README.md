# Instalasi

Pastikan sudah di install python 3

Clone project domain-request
```bash
git clone https://gitlab.com/BayuWrdani/domain-request.git
```
masuk ke folder clone
```bash
cd domain-request
```
Install Library
```bash
pip install -r requierments.txt
```
Mulai Melakukan Migrations
```bash
python manage.py makemigrations
python manage.py migrate
```

Buat Superuser User

```bash
python manage.py createsuperuser
```
silahkan isi dengan apa yang diminta


