from django.shortcuts import render
from django.http import (HttpResponse,HttpResponseRedirect)
from domain.models import (Request)

import json
# Create your views here.
def check_domain(request,nama_domain):
	# nama_domain = request.GET.get('nama_domain')
	status = {
		'status' : False,
		'pesan':''
	}

	if nama_domain:
		nama_domain = nama_domain.lower() # ubah ke lowe
		domain = Request.objects.filter(nama_domain=nama_domain)
		if domain.exists():
			domain = domain.first()
			status['status'] = True
			status['pesan'] = domain.as_object()
		else:
			status['pesan'] = "Domain dengan nama `{}` tidak dapat ditemukan".format(nama_domain)
	else:
		status['pesan'] = "Tidak Ada Nama Domain"

	return HttpResponse(json.dumps(status))


def to_admin(request):
	return HttpResponseRedirect('/admin/')