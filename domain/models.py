from django.db import models
from django.contrib.auth.models import (User)

# Create your models here.
class Request(models.Model):
	STATUS = (
			('disetujui','DI SETUJUI'),
			('ditolak','DI TOLAK'),
			('draf','DRAF')
		)

	nama_domain = models.CharField(max_length=255)
	status = models.CharField(choices=STATUS,default='draf',max_length=255)
	created_by = models.ForeignKey(User,on_delete=models.CASCADE,blank=True,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	def as_object(self):
		obj = {
			"nama_domain":self.nama_domain,
			"status":self.status,
			"created_by":str(self.created_by),
			"created_at":str(self.created_at)
		}
		print (obj)
		return obj

	def __str__(self):
		return "{} - {}".format(self.nama_domain,self.created_by)

	class Meta:
		permissions = [('verify_domain_request', 'Verify Domain Request')]
