from django.contrib import admin
from django.contrib.messages import constants
from django.contrib import messages

from domain.models import (Request)

def diterima_request_domain(modeladmin,request,queryset):
	total = queryset.count()
	qs = queryset
	if qs.exists():
		qs.update(status='disetujui')
		# messages.success(request, 'Berhasil verifikasi {} domain'.format(total))
		messages.add_message(request, constants.SUCCESS, 'Berhasil verifikasi {} domain'.format(total))

	else:
		messages.add_message(request, constants.INFO, 'Tidak ada data draf')

def ditolak_request_domain(modeladmin,request,queryset):
	total = queryset.count()
	qs = queryset
	if qs.exists():
		qs.update(status='ditolak')
		messages.add_message(request, constants.SUCCESS, 'Berhasil verifikasi {} domain'.format(total))
	else:
		messages.add_message(request, constants.INFO, 'Tidak ada data draf')

class RequestAdmin(admin.ModelAdmin):
	list_display = ('nama_domain','status','created_by','created_at')
	search_fields = ('nama_domain',)

	

	actions = [diterima_request_domain,ditolak_request_domain,]

	def get_fieldsets(self,request,obj=None):
		permission = request.user.get_all_permissions()
		if 'domain.verify_domain_request' in permission  or request.user.is_superuser:
			fieldsets = (
		        (None, {
		            'fields': ('nama_domain','status','created_by')
		        }),
		    )
		else:
			fieldsets = (
		        (None, {
		            'fields': ('nama_domain',)
		        }),
		    )
		return fieldsets

	def get_actions(self,request):
		ac = super().get_actions(request)
		permission = request.user.get_all_permissions()
		if 'domain.verify_domain_request' in permission  or request.user.is_superuser:
			return ac
		else:
			return []

	def get_queryset(self,request):
		qs = super().get_queryset(request)

		permission = request.user.get_all_permissions()
		if 'domain.verify_domain_request' in permission  or request.user.is_superuser:
			return qs
		return qs.filter(created_by=request.user)

	def save_model(self, request, obj, form, change):
		if not change:
			obj.created_by = request.user
		super(RequestAdmin, self).save_model(request, obj, form, change)

admin.site.register(Request,RequestAdmin)
